package mx.unam.ciencias.icc;

/**                                                                                               
 * Enumeración para las estructuras de datos del proyecto.   
 */
public enum Estructuras {

    /** El nombre del estudiante. */
    LISTA,
    /** El número de cuenta del estudiante. */
    PILA,
    /** El promedio del estudiante. */
    COLA,
    /** La edad del estudiante. */
    ARBOLBINARIO,
    ARBOLBINARIOORDENADO,
    ARBOLROJINEGRO,
    ARBOLAVL,
    GRAFICA,
    MONTICULOMINIMO;

    /**                                                                                            
     * Regresa una representación en cadena del campo para ser usada en                            
     * interfaces gráficas.                                                                        
     * @return una representación en cadena del campo.                                             
     */
    @Override public String toString() {
	switch(this){
          case NOMBRE: return "Nombre";
          case CUENTA: return "# Cuenta";
          case PROMEDIO: return "Promedio";
          case EDAD: return "Edad";
          default: throw new IllegalArgumentException();
	}
    }
}
